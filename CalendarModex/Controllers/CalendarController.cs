﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CalendarModex.Models;

namespace CalendarModex.Controllers
{
    public class CalendarController : Controller
    {

        public ActionResult Index()
        {
            return View(Person.GetPeople());
        }

        /// <summary>
        /// Listing Events.
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            return View(Event.GetEvents());
        }

        /// <summary>
        /// Gives a list of Events, with given start and end bounds, for a specific person.
        /// </summary>
        /// <param name="start">Start datetime in unix format.</param>
        /// <param name="end">End datetime in unix format.</param>
        /// <param name="personId">ID of person the calendar is for.</param>
        /// <returns>Events in JSON.</returns>
        [HttpPost]
        public JsonResult GetEventsForCalendar(long start, long end, int personId)
        {
            List<Event> events = Event.GetEvents(start, end, personId);
            List<JsonEvent> jsonEvents = new List<JsonEvent>();
            foreach (Event evt in events)
            {
                jsonEvents.Add(new JsonEvent
                {
                    title = evt.Description,
                    start = evt.StartDate.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                    end = evt.EndDate.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                    allDay = false
                });
            }
            return Json(jsonEvents);
        }

        /// <summary>
        /// Get conflicts, with given start and end bounds, for a specific person (or calendar).
        /// </summary>
        /// <param name="start">Start datetime in unix format.</param>
        /// <param name="end">End datetime in unix format.</param>
        /// <param name="personId">ID of person the calendar is for.</param>
        /// <returns>Conflicts in JSON.</returns>
        public ActionResult GetCalendarConflicts(long start, long end, int personId)
        {
            List<Conflict> conflicts = new List<Conflict>();
            List<Event> events = Event.GetEvents(start, end, personId).OrderBy(o => o.StartDate).ToList();
            Event previousEvent = events.First();
            events.Remove(previousEvent);
            foreach(var evt in events)
            {
                if (evt.StartDate < previousEvent.EndDate)
                {
                    conflicts.Add(new Conflict
                    {
                        FirstConflictingEvent = previousEvent,
                        SecondConflictingEvent = evt
                    });
                }
                previousEvent = evt;
            }
            return PartialView("CalendarConflicts", conflicts);
        }

    }
}