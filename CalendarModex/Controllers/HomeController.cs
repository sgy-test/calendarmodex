﻿using CalendarModex.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CalendarModex.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        /// <summary>
        /// Upload CSV file.
        /// </summary>
        /// <param name="file">CSV file</param>
        /// <returns>ActionResult of redirection to Calendar page.</returns>
        public ActionResult UploadCSV(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var filename = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), filename);
                file.SaveAs(path);

                DataHelper.SavePersonData(path);

            }
            return RedirectToAction("Index", "Calendar");
        }

        public ActionResult Calendar()
        {
            return View();
        }

    }
}