namespace CalendarModex.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CalendarModexEntities : DbContext
    {
        public CalendarModexEntities()
            : base("name=CalendarEntities")
        {
        }

        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Person> People { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
