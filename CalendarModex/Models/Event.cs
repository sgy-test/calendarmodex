namespace CalendarModex.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("Event")]
    public partial class Event
    {
        public int EventID { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int PersonID { get; set; }

        public virtual Person Person { get; set; }

        public static List<Event> GetEvents()
        {
            var context = new CalendarModexEntities();
            return context.Events.ToList();
        }

        public static List<Event> GetEvents(long start, long end, int personId)
        {
            var epoch = new DateTime(1970, 1, 1);
            var startDate = epoch.AddSeconds(start);
            var endDate = epoch.AddSeconds(end);

            List<Event> selectedEvents = new List<Event>();
            using (var context = new CalendarModexEntities())
            {
                 selectedEvents =
                    (from e in context.Events
                     where e.StartDate > startDate && e.StartDate <= endDate
                         && e.PersonID == personId
                     select e).ToList();
            }
            return selectedEvents;
        }

    }
}
