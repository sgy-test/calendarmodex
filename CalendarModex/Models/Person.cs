namespace CalendarModex.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("Person")]
    public partial class Person
    {
        public Person()
        {
            Events = new HashSet<Event>();
        }

        public int PersonID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<Event> Events { get; set; }

        public static List<Person> GetPeople()
        {
            using (var context = new CalendarModexEntities())
            {
                return context.People.ToList();
            }
        }
    }
}
