﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarModex.Models
{
    public class Conflict
    {
        public Event FirstConflictingEvent { get; set; }
        public Event SecondConflictingEvent { get; set; }

    }
}