﻿using CalendarModex.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarModex.Helpers
{
    public class DataHelper
    {

        /// <summary>
        /// Delete all people, and their Events from database.
        /// </summary>
        private static void RemoveAllPeople()
        {
            using (var context = new CalendarModexEntities())
            {
                var persistedPersons =
                    from p in context.People
                    select p;

                context.People.RemoveRange(persistedPersons);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Save people in database.
        /// </summary>
        /// <remarks>Makes database empty before saving data.</remarks>
        /// <param name="filename">Filename of CSV file.</param>
        public static void SavePersonData(string filename)
        {
            var newPeople = GetPeopleFromCSV(filename);

            using (var context = new CalendarModexEntities())
            {
                RemoveAllPeople();
                context.People.AddRange(newPeople);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// This function gets lines from a CSV file and process them to generate People from this data.
        /// </summary>
        /// <remarks>
        /// If no lines are returned (empty file), simply no people is generated,
        /// and if the Split() result has not exactly 5 elements, the row is ignored.
        /// </remarks>
        /// <param name="filename">Filename of CSV file.</param>
        /// <returns>List of people from CSV file.</returns>
        private static IEnumerable<Person> GetPeopleFromCSV(string filename)
        {
            var csvData =
                from row in FileHelper.ReadFrom(filename).Cast<string>().Skip(1)
                let columns = row.Split(',')
                where columns.Length == 5
                select new
                {
                    Nom = columns[0],
                    Date = DateTime.Parse(columns[1]),
                    StartTime = DateTime.Parse(columns[2]),
                    EndTime = DateTime.Parse(columns[3]),
                    Description = columns[4]
                };

            List<Person> people = new List<Person>();

            var data = csvData.GroupBy(g => g.Nom);
            foreach (var item in data)
            {
                List<Event> events = new List<Event>();
                foreach (var evt in item)
                {
                    DateTime startDate = evt.Date + evt.StartTime.TimeOfDay;
                    DateTime endDate = evt.Date + evt.EndTime.TimeOfDay;

                    events.Add(new Event {
                        Description = evt.Description,
                        StartDate = startDate,
                        EndDate = endDate
                    });
                }
                people.Add(new Person {
                    Name = item.Key,
                    Events = events
                });
            }
            return people;
        }
    }
}