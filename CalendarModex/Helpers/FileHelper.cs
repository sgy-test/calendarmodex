﻿using CalendarModex.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CalendarModex.Helpers
{
    public class FileHelper
    {

        /// <summary>
        /// Reads file line by line.
        /// </summary>
        /// <param name="file">Fullname of file.</param>
        /// <returns>A list of lines red from file.</returns>
        public static IEnumerable ReadFrom(string file)
        {
            if (File.Exists(file))
            {
                string line;
                using (var reader = File.OpenText(file))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        yield return line;
                    }
                }
            }
            else
            {
                yield break;
            }
        }

    }
}