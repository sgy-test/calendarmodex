﻿/* File managing Index.cshtml DOM */

$(document).ready(
    function () {
        $('.fileinput').fileinput();

        var disableButton = function () {
            $('#import-button').attr("disabled", "disabled");
        };

        var enableButton = function () {
            $('#import-button').removeAttr("disabled");
        };

        disableButton();

        $('.fileinput').on('change.bs.fileinput', enableButton);
        $('.fileinput').on('clear.bs.fileinput', disableButton);
        $('.fileinput').on('reset.bs.fileinput', disableButton);

    }
);