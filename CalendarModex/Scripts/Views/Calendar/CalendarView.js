﻿$(document).ready(function () {

    $("[data-id]").first().addClass('active');

    $('[data-id]').on('click', function (event) {
        $('.nav-pills>li').removeClass('active');
        $(this).addClass('active');
        $("#conflicts-container").html("");
        $("#conflicts-container").hide();
        $('#calendar').fullCalendar('refetchEvents');
    });

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaWeek'
        },
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        buttonText: { month: 'Mois', week: 'Semaine' },
        dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        editable: false,
        events: function (start, end, callback) {
            var elements = $('li.active');
            var element = elements.length === 1 ? elements[0] : $('.nav-pills>li')[0];
            var selectedPerson = element.getAttribute('data-id');

            var data = {
                start: Math.round(start.getTime() / 1000),
                end: Math.round(end.getTime() / 1000),
                personId: selectedPerson
            };

            var jsonSerialized = JSON.stringify(data);

            $.ajax({
                url: '/Calendar/GetEventsForCalendar',
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: jsonSerialized,
                success: function (d) {
                    callback(d);
                }
            });
        }
    });
    $('#calendar').fullCalendar('gotoDate', 2013, 11);

    $("#btnValidate").on('click', function () {
        var elements = $('li.active');
        var element = elements.length === 1 ? elements[0] : $('.nav-pills>li')[0];
        var selectedPerson = element.getAttribute('data-id');

        var view = $("#calendar").fullCalendar('getView');
        var data = {
            start: Math.round(view.start.getTime() / 1000),
            end: Math.round(view.end.getTime() / 1000),
            personId: selectedPerson
        };
        var jsonSerialized = JSON.stringify(data);

        $.ajax({
            url: '/Calendar/GetCalendarConflicts',
            type: 'POST',
            dataType: 'html',
            contentType: 'application/json; charset=utf-8',
            data: jsonSerialized,
            success: function (result) {
                $("#conflicts-container").html(result);
                $("#conflicts-container").show();
            }
        });
    });

});